class Roller
  attr_reader :number, :difficulty, :rolls, :rerolls

  def initialize(number, difficulty, explode = true)
    @number = number
    @difficulty = difficulty
    @rolls = roll(number)

    @rerolls = []
    if explode
      @rerolls = reroll(@rolls)
    end
  end

  def successes
    @rolls.count { |r| r >= @difficulty }
  end

  def failures
    @rolls.count { |r| r == 1 }
  end

  def extra_successes
    @rerolls.count { |r| r >= @difficulty }
  end

  def check
    self.successes + self.extra_successes - self.failures
  end

  def success?
    self.check > 0
  end

  def failure?
    self.check == 0
  end

  def botch?
    self.check < 0
  end

  def roll(number)
    Array.new(number) { rand(1..10) }
  end

  def tens(rolls)
    rolls.count { |r| r == 10 }
  end

  def reroll(rolls)
    if tens(rolls) > 0
      r = roll(tens(rolls))
      r + reroll(r)
    else
      []
    end
  end
end
