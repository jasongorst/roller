require 'cksh_commander'

module Coin
  class Command < CKSHCommander::Command
    set token: ENV['BOT_TOKEN']

    # No subcommand
    # SLACK: /coin
    desc '[reason]', 'Flips a coin.'
    def ___(text)
      flip = %w[Heads Tails].sample

      respond_in_channel!
      add_response_attachment({
          text: "<@#{data.user_name}> flips a coin #{text}",
          fields: [
              {
                  title: 'Result',
                  value: "#{flip}!",
                  short: true
              }
          ],
          mrkdwn_in: ['text'], color: 'good'
      })
    end
  end
end
