require_relative "../../lib/roller.rb"
require_relative "../../lib/dice.rb"
require_relative "../../lib/dice_parser.rb"
require "cksh_commander"

module Roll
  MAX_SHORT_FORMAT_LENGTH = 12
  DEFAULT_DIFFICULTY = 6
  MAX_DICE = 20

  class Command < CKSHCommander::Command
    set token: ENV['BOT_TOKEN']

    # Subcommand coin
    # SLACK: /roll coin [reason]
    desc "coin [reason]", "Flips a coin."
    def coin(text)
      flip = %w[Heads Tails].sample

      respond_in_channel!
      add_response_attachment({
          text: "<@#{data.user_name}> flips a coin #{text}",
          fields: [
              {
                  title: "Result",
                  value: "#{flip}!",
                  short: true
              }
          ],
          mrkdwn_in: ['text'], color: 'good'
      })
    end

    # Subcommand dice
    # SLACK: /roll dice [number]d[sides][+/-modifier] [reason]
    desc "dice [number]d[sides][+/-modifier] [reason]", "Rolls some dice."
    def dice(d, text)
      num, sides, mod = DiceParser::parse(d)
      dice = Dice.new(num, sides, mod)

      mod_str = (mod.to_i == 0 ? '' : sprintf('%+d', mod))

      respond_in_channel!
      add_response_attachment({
          text: "<@#{data.user_name}> rolls *#{num}d#{sides}#{mod_str}* #{text}",
          fields: [
              {
                  title: "Rolls",
                  value: "#{dice.results}",
                  short: dice.results.count <= MAX_SHORT_FORMAT_LENGTH
              },
              {
                  title: "Total",
                  value: "#{dice.total}",
                  short: true
              }
          ],
          mrkdwn_in: ['text'], color: 'good'
      })

      rescue => e
        add_response_attachment({text: e, color: 'warning'})
    end

    # Subcommand noexplode
    # SLACK: /roll noexplode [number of dice] [difficulty] [reason]
    desc "noexplode [number of dice] [difficulty] [reason]", "Rolls some dice without exploding 10s."
    def noexplode(args)
      args = args.split
      num, diff, text = parse_args(args)
      perform_roll(num, diff, text, false)
    end

    # Subcommand explode
    # SLACK: /roll explode [number_of_dice] [difficulty] [reason]
    desc "explode [number of dice] [difficulty] [reason]", "Rolls some dice with exploding 10s."
    def explode(args)
      args = args.split
      num, diff, text = parse_args(args)
      perform_roll(num, diff, text, true)
    end

    # No subcommand
    def ___(args)
      args = args.split
      if args[0] =~ /[dD]\d+/
        d = args.shift
        text = args.join(' ')
        dice(d, text)
      else
        num, diff, text = parse_args(args)
        perform_roll(num, diff, text, false)
      end
    end

    private

    def parse_args(args)
      num = args.shift
      diff = args.shift || DEFAULT_DIFFICULTY
      text = args.join(' ')
      [num, diff, text]
    end

    def perform_roll(number, difficulty, text, explode)
      number = number.to_i

      if number > MAX_DICE
        send_max_number_warning
        return
      end

      if number < 1
        send_min_number_warning
        return
      end

      begin
        difficulty = Integer(difficulty)
      rescue ArgumentError
        text = [difficulty, text].join(" ")
        difficulty = DEFAULT_DIFFICULTY
      end

      if difficulty > 10
        send_max_difficulty_warning
        return
      end

      if difficulty < 2
        send_min_difficulty_warning
        return
      end

      r = Roller.new(number, difficulty, explode)

      if r.botch?
        result = "_BOTCH!_"
        color = 'danger'
      elsif r.failure?
        result = "Failure"
        color = 'warning'
      else
        result = "Successes: *#{r.check}*"
        color = 'good'
      end

      response = {
        text: "<@#{data.user_name}> rolls *#{number}* dice (diff *#{difficulty}*) #{text}",
        mrkdwn_in: ['text'], color: color
      }

      fields = if r.rerolls.empty?
                  {
                    fields: [
                      {
                        title: "Rolls",
                        value: "#{r.rolls}",
                        short: r.rolls.count <= MAX_SHORT_FORMAT_LENGTH
                      },
                      {
                        title: "Result",
                        value: "#{result}",
                        short: true
                      }
                    ]
                  }
               else
                  {
                    fields: [
                      {
                        title: "Rolls",
                        value: "#{r.rolls}",
                        short: r.rolls.count <= MAX_SHORT_FORMAT_LENGTH
                      },
                      {
                        title: "Rerolls",
                        value: "#{r.rerolls}",
                        short: r.rerolls.count <= MAX_SHORT_FORMAT_LENGTH
                      },
                      {
                        title: "Result",
                        value: "#{result}",
                        short: true
                      }
                    ]
                  }
               end

      response = response.merge!(fields)

      respond_in_channel!
      add_response_attachment(response)
    end

    def send_max_number_warning
      add_response_attachment({
          text: "That's too many dice. Use #{MAX_DICE} or less."
      })
    end

    def send_min_number_warning
      add_response_attachment({
          text: "I need a number of dice to roll."
      })
    end

    def send_max_difficulty_warning
      add_response_attachment({
          text: "You can't roll higher than 10."
      })
    end

    def send_min_difficulty_warning
      add_response_attachment({
          text: "The difficulty has to be at least 2."
      })
    end
  end
end
