require 'sinatra/base'
require 'sinatra/custom_logger'
require 'logger'
require 'cksh_commander'
require 'dotenv'

Dotenv.load '.env.production'

CKSHCommander.configure do |c|
  c.commands_path = File.expand_path("../commands", __FILE__)
end

class RollerApp < Sinatra::Base
  helpers Sinatra::CustomLogger

  configure :development, :production do
    logger = Logger.new(File.open("#{root}/log/#{environment}.log", 'a'))
    logger.level = Logger::INFO
    logger.level = Logger::DEBUG if development?
    set :logger, logger

    set :sessions, secret: ENV['SESSION_SECRET'] if production?
  end

  post "/" do
    content_type :json

    # chop command off front of text if slack is still buggy on mobile
    if params["text"].start_with? params["command"]
      params["text"].delete_prefix!(params["command"]).lstrip!
    end

    command = params["command"][1..-1]

    response = CKSHCommander::Runner.run(command, params)
    JSON.dump(response.serialize)
  end
end
